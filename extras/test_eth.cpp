#include <Arduino.h>
#ifndef ETH_PHY_ADDR
  #define ETH_PHY_ADDR    1
#endif

#ifndef ETH_PHY_TYPE
  #define ETH_PHY_TYPE    ETH_PHY_LAN8720
#endif

#ifndef ETH_PHY_POWER
  #define ETH_PHY_POWER   16
#endif

#ifndef ETH_PHY_MDC
  #define ETH_PHY_MDC     23
#endif

#ifndef ETH_PHY_MDIO
  #define ETH_PHY_MDIO    18
#endif

#ifndef ETH_CLK_MODE
  #define ETH_CLK_MODE    ETH_CLOCK_GPIO0_IN
#endif

#include <ETH.h>
#include <WiFi.h>

#ifndef SHIELD_TYPE
  #define SHIELD_TYPE     "ETH_PHY_LAN8720"
#endif

#include <HTTPSServer.hpp>
#include <SSLCert.hpp>
#include <HTTPRequest.hpp>
#include <HTTPResponse.hpp>

using namespace httpsserver;

IPAddress mySN(255, 255, 255, 0);
IPAddress myIP(192, 168, 0, 111);
IPAddress myGW(192, 168, 0, 1);
IPAddress myDNS(8, 8, 8, 8);

SSLCert * cert;
HTTPSServer * secureServer;

bool connected = false;

void eth_handler(WiFiEvent_t event)
{
  switch(event)
  {
    case SYSTEM_EVENT_ETH_START:
      Serial2.println("ETH Started");
      ETH.setHostname("WT32-ETH01");
      break;
    case SYSTEM_EVENT_ETH_CONNECTED:
      Serial2.println("Connected");
      break;
    case SYSTEM_EVENT_ETH_STOP:
      Serial2.println("STOP");
    case SYSTEM_EVENT_ETH_DISCONNECTED:
      Serial2.println("Disconnected");
      connected = false;
      break;
    case SYSTEM_EVENT_ETH_GOT_IP:
      if (!connected) {
          Serial2.println(ETH.macAddress());
          Serial2.println(ETH.localIP());
          connected = true;
      }
      break;
    default:
      break;
  }
}

void setup() {
  Serial2.begin(115200);
  Serial2.println("Creating CERTIFICATE");

  cert = new SSLCert();
  int createCertResult = createSelfSignedCert(
    *cert,
    KEYSIZE_1024,
    "CN=myesp.local,O=acme,C=US");
   
  if (createCertResult != 0) {
    Serial2.printf("Error generating certificate");
    return; 
  }
  Serial2.println("Certificate created with success");
   
  secureServer = new HTTPSServer(cert);
  WiFi.onEvent(eth_handler);
  ETH.begin(ETH_PHY_ADDR, ETH_PHY_POWER);
  //ETH.config(myIP, myGW, mySN, myDNS);

  while(!connected) {
    delay(100);
  }

  Serial2.println("Connected");

  ResourceNode * nodeRoot = new ResourceNode("/", "GET", [](HTTPRequest * req, HTTPResponse * res){
    res->println("Secure Hello World!!!");
  });
 
  secureServer->registerNode(nodeRoot);
 
  secureServer->start();
   
   Serial2.println("Starting server");

  if (secureServer->isRunning()) {
    Serial2.println("Server ready.");
  }
}
 
void loop() {
  
  secureServer->loop();
  
  delay(1);
}

